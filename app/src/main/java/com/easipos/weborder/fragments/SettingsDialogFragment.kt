package com.easipos.weborder.fragments

import android.app.Dialog
import android.os.Bundle
import android.view.ViewGroup
import com.easipos.weborder.R
import com.easipos.weborder.activities.main.MainActivity
import com.easipos.weborder.base.CustomBaseDialogFragment
import com.easipos.weborder.tools.Preference
import com.easipos.weborder.util.urlRegex
import io.github.anderscheow.library.kotlinExt.withContext
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.BaseRule
import io.github.anderscheow.validator.rules.common.NotBlankRule
import io.github.anderscheow.validator.rules.common.RegexRule
import kotlinx.android.synthetic.main.dialog_fragment_settings.*

class SettingsDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(): SettingsDialogFragment {
            return SettingsDialogFragment()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.setCanceledOnTouchOutside(false)
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_settings

    override fun init() {
        super.init()
        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        val url = Preference.prefUrl
        if (url.isNotBlank()) {
            text_input_edit_text_url.setText(url)
        } else {
            text_input_edit_text_url.setText("https://")
        }
        text_input_edit_text_initial_scale.setText(Preference.prefZoomLevel.toString())
    }

    private fun setupListeners() {
        button_ok.setOnClickListener {
            attemptSaveConfiguration()
        }
    }

    private fun attemptSaveConfiguration() {
        withContext { context ->
            val urlValidation = Validation(text_input_layout_url)
                .add(NotBlankRule(R.string.error_field_required))
                .add(RegexRule(urlRegex, R.string.error_invalid_url))
            val zoomLevelValidation = Validation(text_input_layout_initial_scale)
                .add(NotBlankRule(R.string.error_field_required))
                .add(object : BaseRule(R.string.error_invalid_zoom_level) {
                    override fun validate(value: Any?): Boolean {
                        (value as? String)?.toIntOrNull()?.let {
                            return it >= 100
                        } ?: run {
                            return false
                        }
                    }
                })

            Validator.with(context)
                .setListener(object : Validator.OnValidateListener {
                    override fun onValidateFailed(errors: List<String>) {
                    }

                    override fun onValidateSuccess(values: List<String>) {
                        val url = values[0]
                        val zoomLevel = values[1]

                        Preference.prefUrl = url
                        Preference.prefZoomLevel = zoomLevel.toIntOrNull() ?: 100

                        (activity as? MainActivity)?.loadUrl()
                        dismissAllowingStateLoss()
                    }
                }).validate(urlValidation, zoomLevelValidation)
        }
    }
}