package com.easipos.weborder.di.modules

import org.kodein.di.Kodein

fun provideFragmentModule() = Kodein.Module("fragmentModule") {
}
