package com.easipos.weborder.di.modules

import com.easipos.weborder.activities.main.navigation.MainNavigation
import com.easipos.weborder.activities.main.navigation.MainNavigationImpl
import com.easipos.weborder.activities.splash.navigation.SplashNavigation
import com.easipos.weborder.activities.splash.navigation.SplashNavigationImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

fun provideActivityModule() = Kodein.Module("activityModule") {
    bind<SplashNavigation>() with provider { SplashNavigationImpl() }
    bind<MainNavigation>() with provider { MainNavigationImpl() }
}
