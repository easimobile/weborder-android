package com.easipos.weborder.tools

import android.annotation.TargetApi
import android.app.Application
import android.net.Uri
import android.os.Build
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.orhanobut.logger.Logger

class CustomWebViewClient(private val application: Application?, private val listener: WebViewClientListener) : WebViewClient() {

    interface WebViewClientListener {
        fun onReceivedError(failingUrl: String?)
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        Logger.d("shouldOverrideUrlLoading: $url")
        if (shouldOverrideUrlLoading(Uri.parse(url))) {
            view.loadUrl(url)
            return true
        }

        return false
    }

    @TargetApi(Build.VERSION_CODES.N)
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        Logger.d("shouldOverrideUrlLoading: ${request.url}")
        if (shouldOverrideUrlLoading(request.url)) {
            view.loadUrl(request.url.toString())
            return true
        }

        return false
    }

    override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
        Logger.d("onReceivedError: $failingUrl")
        listener.onReceivedError(failingUrl)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Logger.d("onReceivedError: ${request?.url?.toString()}, error: ${error?.errorCode}")
        } else {
            Logger.d("onReceivedError: ${request?.url?.toString()}")
        }
        listener.onReceivedError(request?.url.toString())
    }

    private fun shouldOverrideUrlLoading(uri: Uri): Boolean {
//        if (uri.pathSegments.contains("user") && uri.pathSegments.contains("login")) {
//            (application as? Axstarzy)?.restartAndGotoOnboarding()
//            return false
//        }
        return true
    }
}