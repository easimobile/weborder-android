package com.easipos.weborder.activities.main

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.webkit.CookieManager
import android.webkit.WebSettings
import com.easipos.weborder.R
import com.easipos.weborder.activities.main.mvp.MainPresenter
import com.easipos.weborder.activities.main.mvp.MainView
import com.easipos.weborder.activities.main.navigation.MainNavigation
import com.easipos.weborder.base.CustomBaseAppCompatActivity
import com.easipos.weborder.fragments.SettingsDialogFragment
import com.easipos.weborder.tools.CustomWebViewClient
import com.easipos.weborder.tools.Preference
import com.easipos.weborder.util.urlRegex
import io.github.anderscheow.library.kotlinExt.TAG
import io.github.anderscheow.library.kotlinExt.delay
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<MainNavigation>()

    private val presenter by lazy { MainPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        web_view.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        web_view.restoreState(savedInstanceState)
    }

    override fun onBackPressed() {
        if (web_view.canGoBack()) {
            web_view.goBack()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            delayLoadUrl()
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Action Methods
    fun loadUrl() {
        setupWebView()

        val url = Preference.prefUrl
        val regex = urlRegex.toRegex()
        if (url.isNotBlank() && regex.containsMatchIn(url)) {
            web_view.loadUrl(url)
        } else {
            val fragment = SettingsDialogFragment.newInstance()
            fragment.show(supportFragmentManager, SettingsDialogFragment.TAG)
        }
    }

    private fun delayLoadUrl() {
        delay(3000) {
            loadUrl()
        }
    }

    private fun setupWebView() {
        web_view.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().setAcceptThirdPartyCookies(this, true)
            } else {
                CookieManager.getInstance().setAcceptCookie(true)
            }

            this.webViewClient = CustomWebViewClient(application, object : CustomWebViewClient.WebViewClientListener {
                override fun onReceivedError(failingUrl: String?) {
                }
            })
            this.settings?.apply {
                this.loadWithOverviewMode = true
                this.useWideViewPort = false
                this.loadsImagesAutomatically = true
                this.builtInZoomControls = true
                this.displayZoomControls = false
                this.layoutAlgorithm = WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING
                this.javaScriptEnabled = true
            }
            this.setInitialScale(Preference.prefZoomLevel)
            this.setOnKeyListener { _, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_BACK -> if (web_view.canGoBack()) {
                            web_view.goBack()
                            return@setOnKeyListener true
                        }
                    }
                }
                return@setOnKeyListener false
            }
        }
    }
    //endregion
}
