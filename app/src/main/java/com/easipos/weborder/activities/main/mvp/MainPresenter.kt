package com.easipos.weborder.activities.main.mvp

import android.app.Application
import com.easipos.weborder.base.Presenter

class MainPresenter(application: Application)
    : Presenter<MainView>(application)
