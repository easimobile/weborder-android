package com.easipos.weborder.activities.splash.mvp

import com.easipos.weborder.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
