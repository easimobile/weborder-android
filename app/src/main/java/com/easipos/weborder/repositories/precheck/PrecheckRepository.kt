package com.easipos.weborder.repositories.precheck

import com.easipos.weborder.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckRepository {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
