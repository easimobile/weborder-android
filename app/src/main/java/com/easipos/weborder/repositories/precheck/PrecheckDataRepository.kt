package com.easipos.weborder.repositories.precheck

import com.easipos.weborder.api.requests.precheck.CheckVersionRequestModel
import com.easipos.weborder.datasource.DataFactory
import io.reactivex.Single

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
