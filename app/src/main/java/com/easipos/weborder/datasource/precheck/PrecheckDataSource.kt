package com.easipos.weborder.datasource.precheck

import com.easipos.weborder.api.misc.SingleObserverRetrofit
import com.easipos.weborder.api.requests.precheck.CheckVersionRequestModel
import com.easipos.weborder.api.services.Api
import com.easipos.weborder.executor.PostExecutionThread
import com.easipos.weborder.executor.ThreadExecutor
import com.easipos.weborder.use_cases.error
import com.easipos.weborder.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Single

class PrecheckDataSource(private val api: Api,
                         private val threadExecutor: ThreadExecutor,
                         private val postExecutionThread: PostExecutionThread) : PrecheckDataStore {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        Single.create { emitter ->
            api.checkVersion(model.toFormDataBuilder().build())
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<Boolean>() {
                    override fun onResponseSuccess(responseData: Boolean) {
                        Logger.i("Version checked: $responseData")

                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
