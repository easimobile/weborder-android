package com.easipos.weborder.datasource

import android.app.Application
import com.easipos.weborder.api.services.Api
import com.easipos.weborder.datasource.precheck.PrecheckDataSource
import com.easipos.weborder.datasource.precheck.PrecheckDataStore
import com.easipos.weborder.executor.PostExecutionThread
import com.easipos.weborder.executor.ThreadExecutor

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)
}
