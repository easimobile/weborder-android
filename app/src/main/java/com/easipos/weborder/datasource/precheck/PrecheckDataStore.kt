package com.easipos.weborder.datasource.precheck

import com.easipos.weborder.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckDataStore {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
