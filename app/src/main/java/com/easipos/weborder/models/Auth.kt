package com.easipos.weborder.models

data class Auth(val token: String)