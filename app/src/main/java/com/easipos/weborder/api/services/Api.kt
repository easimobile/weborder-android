package com.easipos.weborder.api.services

import com.easipos.weborder.api.ApiEndpoint
import com.easipos.weborder.api.misc.ResponseModel
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    fun checkVersion(@Body body: RequestBody): Single<ResponseModel<Boolean>>

    @POST(ApiEndpoint.REGISTER_REMOVE_JPUSH_REG_ID)
    fun registerFcmToken(@Body body: RequestBody): Completable

    @POST(ApiEndpoint.REGISTER_REMOVE_JPUSH_REG_ID)
    fun removeFcmToken(@Body body: RequestBody): Completable
}
