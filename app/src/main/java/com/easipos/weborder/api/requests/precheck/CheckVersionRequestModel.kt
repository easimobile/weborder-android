package com.easipos.weborder.api.requests.precheck

import com.easipos.weborder.BuildConfig
import com.easipos.weborder.api.requests.RequestModel
import okhttp3.MultipartBody

class CheckVersionRequestModel: RequestModel() {

    override fun toFormDataBuilder(): MultipartBody.Builder {
        return super.toFormDataBuilder()
            .addFormDataPart("version", BuildConfig.VERSION_CODE.toString())
            .addFormDataPart("app", "ddcard")
            .addFormDataPart("os", "android")
    }
}