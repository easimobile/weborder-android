package com.easipos.weborder.event_bus

data class NotificationCount(val count: Int)